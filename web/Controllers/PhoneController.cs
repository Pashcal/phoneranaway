﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace web.Controllers
{
    public class PhoneController : Controller
    {
        //
        // GET: /Phone/
        private static bool isConnect;
        private static string check, x, y;
        public ActionResult Index()
        {
            if (Request.Form.Count != 0)
            {
                check = Request.Form["check"];
                x = Request.Form["x"];
                y = Request.Form["y"];
                isConnect = true;
            }
            if (!isConnect || check == "off")
            {
                ViewData.Add("color", "black");
                ViewData.Add("object", "Связь с объектом потеряна!");
                ViewData.Add("x", "—");
                ViewData.Add("y", "—");
                isConnect = false;
            }
            else
            {
                if (check == "in")
                {
                    ViewData.Add("color", "green");
                    ViewData.Add("object", "Объект находится в ИТМО!");
                }
                else
                {
                    ViewData.Add("color", "red");
                    ViewData.Add("object", "Объект скрылся из ИТМО!");
                }
                ViewData.Add("x", x);
                ViewData.Add("y", y);
            }

            return View();
        }
    }
}
