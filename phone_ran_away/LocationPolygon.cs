﻿using System.Device.Location;

namespace phone_ran_away
{
    public class LocationPolygon
    {
        private GeoCoordinate[] poly;
        private double eps = 0.00001;
        private bool isCheckIn;
        public LocationPolygon()
        {
            poly = new GeoCoordinate[4];
            poly[0] = new GeoCoordinate(59.955941, 30.309143);
            poly[1] = new GeoCoordinate(59.957253, 30.307792);
            poly[2] = new GeoCoordinate(59.957551, 30.309798);
            poly[3] = new GeoCoordinate(59.956622, 30.311118);
        }

        public bool CheckIn(GeoCoordinate p)
        {
            isCheckIn = true;
            double res = 0;
            for (int i = 0; i < 4; i++)
            {
                res = Vect(poly[i], poly[i == 3 ? 0 : i + 1]).Latitude * Vect(poly[i], p).Longitude -
                      Vect(poly[i], poly[i == 3 ? 0 : i + 1]).Longitude * Vect(poly[i], p).Latitude;
                if (res > eps)
                {
                    isCheckIn = false;
                    break;
                }
            }
            return isCheckIn;
        }
        private GeoCoordinate Vect(GeoCoordinate a, GeoCoordinate b)
        {
            return new GeoCoordinate(b.Latitude - a.Latitude, b.Longitude - a.Longitude);
        }
    }
}
