﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// Управление общими сведениями о сборке осуществляется с помощью следующего 
// набора атрибутов. Измените значения этих атрибутов для изменения
// сведений о сборке.
[assembly: AssemblyTitle("Phone ran away")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Phone ran away")]
[assembly: AssemblyCopyright("Pashcal ©  2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Если для ComVisible установить значение false, типы в этой сборке не будут поддерживаться 
// COM-компонентами.  При необходимости доступа к какому-либо типу в этой сборке 
// из модели COM задайте для атрибута ComVisible этого типа значение true.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если данный проект видим для COM
[assembly: Guid("848b4e5b-9666-4929-8fa7-f622ff873532")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//      Номер построения
//      Редакция
//
// Можно указать все значения или задать для номеров редакции и построения значения по умолчанию 
// с помощью символа '*', как показано ниже:
[assembly: AssemblyVersion("0.1.0")]
[assembly: AssemblyFileVersion("0.1.0")]
[assembly: NeutralResourcesLanguageAttribute("ru-RU")]
