﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Windows;
using Windows.Devices.Geolocation;
using Windows.System;
using System.Net.Http;

namespace phone_ran_away
{
    public class Location
    {
        private Geolocator me;
        private Geoposition pos;
        private LocationPolygon poly;
        public Location()
        {
            me = new Geolocator() { MovementThreshold = 1, DesiredAccuracyInMeters = 5 };
            poly = new LocationPolygon();
            MeOnStatusChanged(me, null);
            me.PositionChanged += MeOnPositionChanged;
            me.StatusChanged += MeOnStatusChanged;
        }
        public Geoposition Pos
        {
            get { return pos; }
            set { pos = value; }
        }

        public GeoCoordinate GeoPos()
        {
            if (pos != null)
                return GeoConvert(pos.Coordinate);
            else
                return new GeoCoordinate(59.956320, 30.310124);
        }
        private GeoCoordinate GeoConvert(Geocoordinate p)
        {
            return new GeoCoordinate
                (
                p.Latitude,
                p.Longitude,
                p.Altitude ?? Double.NaN,
                p.Accuracy,
                p.AltitudeAccuracy ?? Double.NaN,
                p.Speed ?? Double.NaN,
                p.Heading ?? Double.NaN
                );
        }

        private async void MeOnStatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            if (sender.LocationStatus == PositionStatus.Disabled)
            {
                if (MessageBox.Show("Определение позиции заблокировано. Перейти к настройкам телефона?",
                                    "Ошибка настроек", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    await Launcher.LaunchUriAsync(new Uri("ms-settings-location:"));
                }
                return;
            }
        }

        public void UpdatePos()
        {
            MeOnPositionChanged(me, null);
        }

        private async void MeOnPositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            try
            {
                pos = await sender.GetGeopositionAsync(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(2));
                if (poly.CheckIn(GeoPos()))
                    SendMessage(true);
                else
                    SendMessage(false);

            }
            catch (Exception e)
            {
                MessageBox.Show("Error!", e.Message, MessageBoxButton.OK);
            }
        }

        private async void SendMessage(bool checkIn)
        {
            try
            {
                var url = @"http://5.164.103.218:52497/phone";
                var client = new HttpClient();
                var content = new List<KeyValuePair<string, string>>();
                if (checkIn)
                {
                    content.Add(new KeyValuePair<string, string>("check", "in"));
                }
                else
                {
                    content.Add(new KeyValuePair<string, string>("check", "out"));
                }
                content.Add(new KeyValuePair<string, string>("x", Math.Round(GeoPos().Latitude, 6).ToString()));
                content.Add(new KeyValuePair<string, string>("y", Math.Round(GeoPos().Longitude, 6).ToString()));

                await client.PostAsync(url, new FormUrlEncodedContent(content))
                    .ContinueWith((postTask) =>
                    {
                        postTask.Result.EnsureSuccessStatusCode();
                    });
            }
            catch (Exception e)
            {
                MessageBox.Show("Error!", e.Message, MessageBoxButton.OK);
            }
        }
    }
}
