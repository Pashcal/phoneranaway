﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using phone_ran_away.Resources;

namespace phone_ran_away
{
    public partial class MainPage : PhoneApplicationPage
    {
        private Location location;
        public MainPage()
        {
            InitializeComponent();
            BuildLocalizedApplicationBar();
            location = new Location();
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar() { Opacity = 0.8 };
            ApplicationBarIconButton appBarIcon = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.me.png", UriKind.Relative));
            appBarIcon.Text = AppResources.AppBarIcon;
            appBarIcon.Click += (sender, args) => location.UpdatePos();
            ApplicationBar.Buttons.Add(appBarIcon);
        }
    }
}